#!/usr/bin/env perl
# Copyright (c) <2013>, <mateusz@serveraptor.com>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
# ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use strict;
use warnings;

my $version = "0.1";

use Net::SNMP;
use Getopt::Std;

my $hostname;
my $verbose = 0;
my $summary = 0;
my $failure = 0;
my $community = "public"
my $oid = "1.3.6.1.2.1.15.3.1";
my $logfile = "/var/log/bgp-mon.log";
my $logging = 0;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,
$yday,$isdst)=localtime(time);
$mon+=1;
$year+=1900;
my $timestamp = sprintf("%02d/%02d/%04d %02d:%02d:%02d", $mday,$mon,$year,$hour,$min,$sec);


my %options=();
getopts("vshlH:c:o:", \%options);


# HashTable containing information about the connection with BGP peer 
# Documentation: http://tools.cisco.com/Support/SNMP/do/BrowseOID.do?local=en&translate=Translate&objectInput=1.3.6.1.2.1.15.3.1
my %bgpPeer = (
	Identifier => 0,
	State => 0,
	AdminStatus => 0,
	LocalAddr => 0,
	LocalPort => 0,
	RemoteAddr => 0,
	RemotePort => 0,
	RemoteAS => 0,
	InUpdates => 0,
	OutUpdates => 0,
	InTotalMessages => 0,
	OutTotalMessages => 0,
	LastError => 0,
	FsmEstablishedTransitions => 0,
	FsmEstablishedTime => 0,
	ConnectRetryInterval => 0,
	HoldTime => 0,
	KeepAlive => 0,
	HoldTimeConfigured => 0,
	KeepAliveConfigured => 0,
	MinASOriginationInterval => 0,
	MinRouteAdvertismentInterval => 0,
	InUpdateElapsedTime => 0,
	Faulty => 0,
);

my (%bgpTable, %bgpPeers, $snmp, $peer);

sub printHelp {
	print("BGP Monitor ver $version.\n");
	print("This program retrives informations about BGP from border routers via SNMP.
It checks if any neighbor is in state other than configured. It also prints summary if -s provided.\n");
	print("Usage: $0 [options] <-H hostname>
\t-h\tPrints this help message
\t-H\tHostname
\t-o\tBase OID (Default: $oid)
\t-c\tSNMP community
\t-s\tPrint summary
\t-l\tWrite to logfile ($logfile)
\t-v\tBe verbose\n");
}

if ($options{h}) { printHelp(); exit(1); }
if ($options{H}) { $hostname=$options{H} } else { printHelp(); exit(1); }
if ($options{c}) { $community=$options{c} }
if ($options{o}) { $oid=$options{o} }
if ($options{s}) { $summary=1 }
if ($options{v}) { $verbose=1 }
if ($options{l}) { $logging=1 }

if ($logging == 1) {
	open(LOGFILE, ">> $logfile") || die "Error: Cannot open logfile: $logfile!\n";
}

sub writelog {
	my $logline = shift;
	if($logging == 1) {
		#print LOGFILE "bgpmon $timestamp $hostname # ",$logline,"\n";
	}
	if($verbose ==1 ) {
		print "$timestamp $logline\n";
		print "$logline, ";
	}
}
sub snmpConnect {
	# Let's create SNMP session!
	my ($session, $error) = Net::SNMP->session(
		-hostname	=> shift,
		-community => shift, 
	);
	if(!$session) {
		printf("Error: %s.\n", $error);
		exit(1);
	}
	return $session;
}

sub snmpDisconnect {
	my $session = shift;
	$session->close();
}


sub snmpGetBgpTable {
	my $session = shift;
	my $snmpResult = $session->get_table(-baseoid => $oid,);		
	if(!$snmpResult) {
		printf("Error: %s.\n", $session->error());
		exit(1);
	}
	return %{$snmpResult};
}

sub makeBgpPeersTable {
	foreach my $key (keys %bgpTable) {
		if($key =~ /$oid\.7\.(.*)/) {
			my $ip = $bgpTable{$key};
			# $oid.NUM.$ip
			$bgpPeers{$bgpTable{$key}}{'Identifier'} = $bgpTable{"$oid.1.$ip"};
			$bgpPeers{$bgpTable{$key}}{'State'} = $bgpTable{"$oid.2.$ip"};
			$bgpPeers{$bgpTable{$key}}{'AdminStatus'} = $bgpTable{"$oid.3.$ip"};
			$bgpPeers{$bgpTable{$key}}{'NegotiatedVersion'} = $bgpTable{"$oid.4.$ip"};
			$bgpPeers{$bgpTable{$key}}{'LocalAddr'} = $bgpTable{"$oid.5.$ip"};
			$bgpPeers{$bgpTable{$key}}{'LocalPort'} = $bgpTable{"$oid.6.$ip"};
			$bgpPeers{$bgpTable{$key}}{'RemoteAddr'} = $bgpTable{"$oid.7.$ip"};
			$bgpPeers{$bgpTable{$key}}{'RemotePort'} = $bgpTable{"$oid.8.$ip"};
			$bgpPeers{$bgpTable{$key}}{'RemoteAS'} = $bgpTable{"$oid.9.$ip"};
			$bgpPeers{$bgpTable{$key}}{'InUpdates'} = $bgpTable{"$oid.10.$ip"};
			$bgpPeers{$bgpTable{$key}}{'OutUpdates'} = $bgpTable{"$oid.11.$ip"};
			$bgpPeers{$bgpTable{$key}}{'InTotalMessages'} = $bgpTable{"$oid.12.$ip"};
			$bgpPeers{$bgpTable{$key}}{'OutTotalMessages'} = $bgpTable{"$oid.13.$ip"};
			$bgpPeers{$bgpTable{$key}}{'LastError'} = $bgpTable{"$oid.14.$ip"};
			$bgpPeers{$bgpTable{$key}}{'FsmEstablishedTransitions'} = $bgpTable{"$oid.15.$ip"};
			$bgpPeers{$bgpTable{$key}}{'FsmEstablishedTime'} = $bgpTable{"$oid.16.$ip"};
			$bgpPeers{$bgpTable{$key}}{'ConnectRetryInterval'} = $bgpTable{"$oid.17.$ip"};
			$bgpPeers{$bgpTable{$key}}{'HoldTime'} = $bgpTable{"$oid.18.$ip"};
			$bgpPeers{$bgpTable{$key}}{'KeepAlive'} = $bgpTable{"$oid.19.$ip"};
			$bgpPeers{$bgpTable{$key}}{'HoldTimeConfigured'} = $bgpTable{"$oid.20.$ip"};
			$bgpPeers{$bgpTable{$key}}{'KeepAliveConfigured'} = $bgpTable{"$oid.21.$ip"};
			$bgpPeers{$bgpTable{$key}}{'MinASOriginationInterval'} = $bgpTable{"$oid.22.$ip"};
			$bgpPeers{$bgpTable{$key}}{'MinRouteAdvertismentInterval'} = $bgpTable{"$oid.23.$ip"};
			$bgpPeers{$bgpTable{$key}}{'InUpdateElapsedTime'} = $bgpTable{"$oid.24.$ip"};
			if( $bgpTable{"$oid.3.$ip"} == 2 && $bgpTable{"$oid.2.$ip"} != 6 ) { 
				$bgpPeers{$bgpTable{$key}}{'Faulty'} = 1;
				$failure = 2;
					#writelog("BGP ERROR - Neighbor $bgpPeers{$bgpTable{$key}}{'RemoteAddr'} state is ".peerState($bgpPeers{$bgpTable{$key}}{'State'}).". Most likely failure.");
					writelog("Padla sesja BGP z $bgpPeers{$bgpTable{$key}}{'RemoteAddr'}. ");
			} else { 
				$bgpPeers{$bgpTable{$key}}{'Faulty'} = 0; 
			}
		} 
	}
}

sub peerAdminStatus {
	my $statusNumber = shift;
	if($statusNumber == 1) { return 'Disabled'; }
	elsif($statusNumber == 2) { return 'Enabled'; }
	else { return 'Unknown'; }
}
sub peerState {
	my $stateNumber = shift;
	if($stateNumber == 1) { return 'Idle'; }
	elsif($stateNumber == 2) { return 'Connect'; }
	elsif($stateNumber == 3) { return 'Active'; }
	elsif($stateNumber == 4) { return 'Opensent'; }
	elsif($stateNumber == 5) { return 'Openconfirm'; }
	elsif($stateNumber == 6) { return 'Established'; }
	else { return 'Unknown'; }
}


$snmp = snmpConnect($hostname, $community);
%bgpTable = snmpGetBgpTable($snmp);
makeBgpPeersTable();
snmpDisconnect($snmp);

format SUMMARY_TOP =
					BGP Summary
+-----------------+-----------+-------------+--------------+-------------------------+---------+
|    Neighbor     |     AS    |    State    | AdminStatus  |     Updates in/out      | Faulty? |
+-----------------+-----------+-------------+--------------+-------------------------+---------+
.

format SUMMARY = 
| @<<<<<<<<<<<<<< | @>>>>>>>> | @>>>>>>>>>> | @>>>>>>>>>>> | @>>>>>>>>> / @<<<<<<<<< | @>>>>>> |
$bgpPeers{$peer}{'RemoteAddr'}, $bgpPeers{$peer}{'RemoteAS'}, peerState($bgpPeers{$peer}{'State'}), peerAdminStatus($bgpPeers{$peer}{'AdminStatus'}), $bgpPeers{$peer}{'InUpdates'}, $bgpPeers{$peer}{'OutUpdates'},$bgpPeers{$peer}{'Faulty'}
.

if($summary==1) {
	print "Getting BGP informations from $hostname.\n";
	$^ = 'SUMMARY_TOP';
	$~ = 'SUMMARY';
	foreach $peer (keys %bgpPeers) {
		write;
	}
}
if($failure == 0) {
	writelog("BGP OK - All neigbors up.");
}
if($logging == 1) {
	close(LOGFILE);
}
exit($failure);
